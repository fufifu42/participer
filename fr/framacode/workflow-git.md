# Méthode de travail git

## Via l'interface web de Framagit (ou Gitlab)

Pour passer l'interface de Framagit en Français, vous devez&nbsp;:

  1. cliquer sur votre avatar (en haut à droite), puis [**Profile**](https://framagit.org/profile)
  * modifier **Preferred language** de **English** à **Français**
  * cliquer sur le bouton **Update profile settings** en bas de la page


### Créer une divergence

Pour commencer nous devons créer une copie du projet. Cela s'appelle **Créer une divergence** (souvent aussi appelé *forker*). Pour ce faire, vous devez :

* vous rendre sur le projet originel, ici https://framagit.org/framasoft/participer
* cliquer sur **Créer une divergence**
* cliquer sur votre pseudo/avatar

Vous serez redirigé sur **votre** copie du code (facilement vérifiable en regardant l'adresse : `https://framagit.org/spf/participer/` -> `https://framagit.org/VOTRE-PSEUDO/participer/` à la place de `https://framagit.org/framasoft/participer`).

### Créer une branche

Créer une branche vous permet de faire une copie de votre copie. Si vous ne pensez pas faire d'autres contributions, vous pouvez passer cette étape et directement travailler sur la branche principale (souvent appelée `master`).

Pour créer une branche :

![code créer une branche](images/code_branche.png)

* vous êtes sur **votre** copie de projet (**1**)
* depuis une divergence (**2**)
* sur la branche principale (**3**)
* cliquez sur **+** (**4**)
* cliquez sur **Nouvelle branche** (**5**)

![code création branche](images/code_creation_branche.png)

Nommez votre branche dans la case **Branch name**. Ici, je l'appelle **framacode-explications** (pas d'espaces ni d'accents). On laisse **master** pour **Create from** puisque nous souhaitons bien la créer depuis la branche existante **master**. On clique alors sur **Create branch**

### Faire un changement (`commit`)

A partir de là, vous pouvez faire les modifications que vous souhaitez. Dans cet exemple, je souhaite remplir la page https://participer.framasoft.org/fr/framacode/workflow-git.html actuellement vide, je vais donc la chercher dans le code en cliquant sur **Dépôt** dans le menu de gauche, puis sur `fr` > `framacode` > `workflow-git.md`. Je clique sur **Éditer** pour ouvrir le fichier et pouvoir faire mes modifications.

![éditer un fichier](images/code_editer_fichier.png)

### Demander à intégrer les changements (`Demandes de fusion`)

Une fois mes modifications faites et prêtes à être envoyées, je remplis le **Message de commit** sous le fichier avec une courte explication de ce que j'ai fait (ici : « Explications de la méthode de travail via l'interface web de framagit ») et je clique sur **Commit changes**.

![git commit changements](images/code_commit_changes.png)

Pour envoyer votre travail sur le code originel, vous devez cliquer sur le bouton **Créer une demande de fusion**.

![demande de fusion](images/demande_fusion.png)

Vous êtes alors redirigé sur la page de fusion :

![MR discussion](images/mr_explications_fullpage.png)

  1. **From** (**depuis**) votre dépôt **into** (**dans**) celui dans lequel vous souhaitez voir vos modifications
  - le titre de vos modifications
  - une explication de ce qu'apporte votre travail
  - **votre** branche source
  - la branche vers laquelle vont les modifications
  - (optionnel) permet de supprimer la branche une fois la demande de fusion acceptée
  - envoi de la demande de fusion
  - une visualisation de vos modifications

### Discuter / rectifier

Si tout va bien du premier coup (déjà, félicitations) votre demande est acceptée et vous pouvez supprimer votre dépôt, sinon, vous devez reprendre depuis [Faire un changement (`commit`)](#faire-un-changement-commit).

Pour supprimer votre dépôt **après que votre demande de fusion ait été acceptée ou refusée**, vous devez aller dans **Paramètres** > **Général** > cliquer sur **Étendre** en face de **Advanced** > **Supprimer le projet**.
