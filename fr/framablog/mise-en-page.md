# Mettre en page un article sur le <b class="violet">Frama</b><b class="rouge">blog</b>

Avant toute chose, il vous faudra vous connecter au <b class="violet">Frama</b><b class="rouge">blog</b> selon l'URL de connexion et les identifiants que nous vous aurons transmis.

**Évidemment, merci de les garder secrets&nbsp;!** N'oubliez pas qu'en termes de sécurité informatique, le gros point faible se trouve entre la chaise et le clavier. ^^

## Fonctionnement de Wordpress

### Pour mieux utiliser l'éditeur d'article
![](images/wordpress-edition.png)
1. Les `Options de l'écran` vous permettent, en cochant et décochant des options, de faire apparaître les cadres correspondants. Pensez à cocher la case "auteur" (décochée par défaut) si jamais vous devez changer l'auteur·trice d'un article. Le faire une seule fois le validera pour toute vos utilisations&nbsp;;
2. Les onglets `Visuel` (par défaut) et `Texte` vous permettent de passer d'un éditeur classique avec les boutons, à un éditeur HTML&nbsp;;
3. Cliquez sur le bouton `Ouvrir/fermer la boite d'outils` afin de profiter d'une deuxième barre de boutons utiles pour nos publications (on le fait une fois, ça reste pour toutes nos futures utilisations&nbsp;!)
4. Le cadre `Publier` est celui où vous avez le plus de risques de faire une boulette&hellip;


#### Le cadre "publier/planifier"&hellip;

Détaillons le cadre **4** de notre image.
Rappel&nbps;: **Toute publication se fait après un accord commun&nbsp;;)**.

* **A** Utilisez le bouton `Enregistrer comme brouillon`, autant de fois que vous voulez, aucun souci, ça enregistre vos modifs avec différentiel, c'est beau la vie&nbsp;;
* **B** Dès qu'un article est publié ou planifié, le bouton vert devient `Mettre à jour`. Le seul moyen de savoir si l'article est publié ou juste planifié, c'est de regarder l'état de l'article&nbsp;;
* **D** Par défaut vous ne touchez JAMAIS au bouton vert `Publier`, sauf si vous voulez publier l'article&nbsp;;
  * Exception&nbsp;: lorsque vous voulez planifier un article, cliquez sur `modifier` dans **C**,  choisissez la date & heure, puis cliquez sur `OK` juste à côté.
  * **Si vous avez réussi**, le bouton vert  `Publier` devient `Planifier`&nbsp;: vous pouvez le cliquer&nbsp;;



### Les copier/coller de la mooooort...

<div class="alert alert-warning">
	<ul>
		<li><strong>TOUJOURS</strong> passer son texte par un éditeur de type Gedit / Bloc-notes / Notepad++ avant de le copier/coller&nbsp;;</li>
		<li><strong>TOUJOURS</strong> utiliser le bouton <code>coller en mode texte</code> de l'éditeur <code>Visuel</code> de wordpress avant de copier/coller&nbsp;;</li>
		<li>Ne <strong>JAMAIS</strong> faire de copier-coller depuis word ou writer ou pire : depuis Framapad.</li>
	</ul>
</div>


![](images/wordpress-cc-bouton.png)

* Wordpress est capricieux&nbsp;: **pas de copier-coller de texte sans avoir coché le bouton  «&nbsp;Coller en texte&nbsp;» auparavant** (qui se trouve dans le menu déroulant "modifier"). Sinon il chope les balises de Framapad/LibreOffice/Word/etc. et ça pourrit le thème et c'est moche&nbsp;;
* Ce bouton se trouve dans la deuxième ligne de boutons l'éditeur `Visuel`, que l'on a ouverte grâce à **3**.
* Pour copier/insérer du html, passer en mode `Texte` avec **2** (par défaut, on est en mode `Visuel`)&nbsp;;

#### _Comment je sais si tout va bien&nbsp;?_
Regardez les sauts de ligne (dans l'éditeur `Visuel` & la prévisualisation de l'article)&nbsp;:

![](images/wordpress-cc-reussi.png)

**S'il y a une petite interligne entre deux paragraphes, tout va bien&nbsp;\o/&nbsp;!**

****

#### _Comment je sais si tout va mal&nbsp;?_

![](images/wordpress-cc-rate.png)

**Si les interlignes sont collés-serrés, y'a un problème et la lecture de l'article sera étouffante (les sauts de lignes deviennent des retours de chariot&nbsp;: pas glop)**

****


![](images/wordpress-div-span.png)

**Si des `div` ou des `span` apparaissent dans l'éditeur `Texte`, y'a un raté.**

Ici, on a fait un simple copier collé depuis Framapad&nbsp;: le code est dégueulasse&nbsp;!

****


![](images/wordpress-brouillon-bad.png)

**Si, en mode aperçu, votre article est étouffé/étouffant, c'est que le copier-coller est raté.**


****

### Comment Wordpress a été pensé

Il a été pensé pour être votre éditeur de base&nbsp;: vous êtes sensé·e·s y écrire vos articles directement sur l'interface, sans avoir besoin de rien d'autre.

Du coup, pour les rebelles comme nous, y'a deux écoles&nbsp;:

* Je fais mon texte en .txt (tout retour à la ligne doit avoir une ligne vide entre 2 paragraphes), je «&nbsp;colle en texte&nbsp;» *dans l'onglet visuel*, j'ajoute liens, images, mise en page (gras, quotes, listes....), readmore, catégories, tags, tweet auto, image à la une => je propose à la publication&nbsp;;
* Je fais mon texte en .html, je le colle *dans l'onglet texte*, j'ajoute les images, readmore, catégories, tags, tweet auto, image à la une => je propose à la publication.



## Mise en forme de l'article

<div class="alert alert-warning">
	<ul>
		<li>Wordpress ajoute automatiquement les insécables, guillemets français, ellipsis, parce qu'on a une super extension pour faire le boulot à notre place et c'est pas du luxe&nbsp;;)&nbsp;;</li>
		<li><strong>TOUJOURS</strong> penser à mettre une balise «&nbsp;read more&nbsp;» après les deux ou trois premières phrases&nbsp;;</li>
		<li>Ne <strong>JAMAIS</strong> faire de copier-coller depuis word ou writer (ça fait plein de <span> tout moches). Utiliser du texte brut ou le bouton «&nbsp;coller en texte brut&nbsp;» dans la seconde ligne des outils de mise en page.</li>
	</ul>
</div>

### Titres

* Le seul «&nbsp;Titre 1&nbsp;» est le titre de l'article&nbsp;;
* On commence donc notre titrage à partir des titres de niveau 2.

### Catégories

* Toujours utiliser au moins une catégorie&nbsp;;
* On peut multi-catégoriser (à vous de voir ^^)&nbsp;;
* La catégorie par défaut `non classés`, doit donc être décochée&nbsp;;
* Toujours cocher la catégorie parente lorsqu'on veut cocher une sous-catégorie.

### Tags

* `RezoTic` et `Planet` permettent d'inclure l'article dans le flux RSS de ces blogs&nbsp;;
* Penser à regarder les `tags les plus utilisés`&nbsp;;
* Utiliser l'auto-complétion&nbsp;;
* Écrire directement un nom de tag dans ce champ (séparer à l'aide de virgules)&nbsp;;
* Valider avec OK&nbsp;;
* Ne pas hésiter à taguer en doublon des catégories/sous catégories (mettre `dégooglisons` ou `GAFAM` dans les tags même si les catégories éponymes sont déjà cochées.

### Publication automatique sur les réseaux

Nous avons installé des plug-in Wordpress pour publier automatiquement les articles sur nos comptes de réseaux sociaux.

* Seuls les nouveaux articles sont publiés, pas leur mises à jour&nbsp;;
* Pas besoin d'avoir les clés du comptes, Wordpress les a.

#### Wordpress to Diaspora*

* Si vous avez correctement mis vos tags wordpress il n'y a RIEN A FAIRE \o/&nbsp;;
* Le plug-in affichera sur Diaspora* l'image à la une de l'article et l'extrait (ce qu'il y a avant la balise Read More) d'où l'importance de ces réglages dans votre article&nbsp;;
* Les tags #framasoft, #framablog, #Libre, #free sont automatiquement ajoutés à votre post Diaspora*&nbsp;;
* le lien vers l'article est ajouté automatiquement.

#### Wordpress to Mastodon
* Il n'y a rien à faire, le titre et l'adresse web de l'article sont automatiquement pouettés sur le compte mastodon de Framasoft lors de la première publication de l'article ;
* Ce pouet sera automatiquement twitté par le compte Twitter de Framasoft.

#### Wordpress to twitter

**OPTIONNEL**&nbsp;: wordpress publie automatiquement un pouet `Titre + URL` sur le compte Mastodon de Framasoft, ce qui est ensuite automatiquement retwitté sur le compte Twitter de Framasoft. Cette option ne sert plus **que si vous voulez ajouter un 2e tweet personnalisé**, par exemple en mentionnant une personne concernée.

* Écrivez votre tweet dans le cadre de la colonne de droite prévu à cet effet&nbsp;;
* Ajoutez les tags et mentions que vous voulez dans la limite des 280 caractères&nbsp;;
* Rajoutez IMPÉRATIVEMENT #url# pour inclure le lien vers l'article&nbps;;
* Votre message doit être différent du titre de de l'article, pour ne pas avoir 2 tweets identiques.

#### Wordpress to Facebook
* Il faut avoir mis une `image à la une` pour que cela publie l'article comme une photo sur la page Facebook de Framasoft.
* Il n'y a rien à faire d'autre, l'article est publié automatiquement lors de la première publications de l'article



### Illustrations

* Toujours choisir une `Image mise en avant` (bas de la colonne de droite) afin que le thème l'utilise dans notre page d'accueil, et pour les réseaux sociaux&nbsp;;
* Utiliser des images sous licence libre ou de libre diffusion (chercher sur [Search CreativeCommons](https://ccsearch.creativecommons.org/))&nbsp;;
* Lors de l'ajout d'une image à un article, mettre en légende sous le format `Titre-Licence-Auteur`. Exemple&nbsp;: `Photo magnifique CC-BY-SA Auteur qui déchire`.

## Utilisateur·trice

L'article vous est attribué par défaut. Il faut donc que vous pensiez à aller dans votre profil utilisateur WordPress pour&nbsp;:

* Mettre à jour votre mini-bio&nbsp;;
* Mettre vos réseaux sociaux&nbsp;;
* Le champ `facebook` est un lien vers votre profil diaspora*/framasphère (oui on l'a hacké ^^)&nbsp;;
* Pensez à mettre une photo de profil (ou image, dessin, icône, etc.).
