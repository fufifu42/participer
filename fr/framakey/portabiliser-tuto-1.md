# Préparons le terrain
## Les outils

Mon père disait toujours : « un bon ouvrier a toujours de bons outils ».

Avant tout, il va falloir vous munir de quelques logiciels qui vous
aideront lors de ce processus. Libre à vous de les utiliser ou de leur
en préférer d’autres ayant les mêmes fonctionnalités.
Évidemment, tous ces logiciels sont libres :)

 *  IconMakerTk : pour créer éditer vos icônes
 *  Notepad++Portable : pour éditer les fichier NSIS et bien plus encore !
 *  NSISPortable : pour compiler les fichier NSIS
 *  RegshotPortable : pour vérifier la discrétion d’une application
 *  XnRessourceEditor (XNREPortable) : éditeur de ressources (vous permettra par exemple d’extraire l’icône d’un logiciel)
 *  FramaWizard : un assistant qui essaie de mâcher le travail
 *  FramaCompress : pour réduire la taille de vos applications

Et comme à Framakey/Framasoft, on est des types sympas, on vous a
préparé un petit zip contenant tout cela…
[Téléchargez le pack de portabilisation](portabiliser-pack.html)

Côté matériel, il vous faut :

 *  au moins une machine avec Windows (idéalement 2 ou plus pour tester)
 *  un support amovible (type clef usb)

## Préparation du plan de travail

Ma mère disait toujours : « Comment tu veux que je fasse la cuisine sans plan de travail ? »

Commencez par vous créer un dossier nommé « portables » (peu importe son nom).
Par exemple `E:\dev\portables` (ici dans `E:\dev` mais c’est vous qui voyez,
c’est le dossier de travail qui sera retenu dans le reste du tutoriel).

Dézippez maintenant le pack de portabilisation fourni plus haut, et
placez (copier ou déplacer) le dossier « Pack » dans `E:\dev\portables` de façon à obtenir `E:\dev\portables\Pack`.

Enfin, téléchargez [PNotes](http://pnotes.sourceforge.net/).
Vous remarquerez que la version à télécharger est un fichier zip,
c’est typiquement le genre de chose qui nous invite à penser que PNotes
fleure bon le logiciel portable ! :)

Passons aux choses sérieuses, [testons la discrétion et la portabilité…](portabiliser-tuto-2.html)  
Vous pouvez aussi [revenir à la page précédente…](portabiliser-tuto-intro.html)
